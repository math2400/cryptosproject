<?php

namespace App\Repository;

use App\Entity\Favori;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Favori|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favori|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favori[]    findAll()
 * @method Favori[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Favori::class);
    }



    public function findAllByUtilisateur($utilisateur_id)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.utilisateur_id = :val')
            ->setParameter('val', $utilisateur_id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findIfIsFav($idCrypto,$utilisateur_id)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.id_crypto = :val1')
            ->andWhere('f.utilisateur_id = :val')
            ->setParameter('val', $utilisateur_id)
            ->setParameter('val1', $idCrypto)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Favori[] Returns an array of Favori objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Favori
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
