<?php

namespace App\Repository;

use App\Entity\Crypto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Crypto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Crypto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Crypto[]    findAll()
 * @method Crypto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CryptoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Crypto::class);
    }

    public function findCryptoById($id){
        return $this->createQueryBuilder('crypto')
            ->andWhere('crypto.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findCryptoByMarketCap2($val1,$val2){
        return $this->createQueryBuilder('crypto')
            ->andWhere('crypto.market_cap < :val1')
            ->andWhere('crypto.market_cap > :val2')
            ->setParameter('val1', $val1)
            ->setParameter('val2', $val2)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findCryptoByMarketCapInf1($val1){
        return $this->createQueryBuilder('crypto')
            ->andWhere('crypto.market_cap < :val1')
            ->setParameter('val1', $val1)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findCryptoByMarketCapSup1($val1){
        return $this->createQueryBuilder('crypto')
            ->andWhere('crypto.market_cap > :val1')
            ->setParameter('val1', $val1)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findIdCrypto($name)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.nom = :val')
            ->setParameter('val', $name)
            ->getQuery()
            ->getResult()
            ;
    }

    public function filtreCrypto($prixmin, $prixmax, $taux,$circulating_supply)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.prix > :prixmin')
            ->andWhere('c.prix < :prixmax')
            ->andWhere('c.price_change_percentage_24h > :taux')
            ->andWhere('c.circulating_supply > :circulating_supply')
            ->setParameter('prixmin', $prixmin)
            ->setParameter('prixmax', $prixmax)
            ->setParameter('taux', $taux)
            ->setParameter('circulating_supply', $circulating_supply)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Crypto[] Returns an array of Crypto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Crypto
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
