<?php

namespace App\Controller;

use App\Entity\Crypto;
use App\Entity\Favori;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/{_locale}")
 */
class FavorisController extends AbstractController
{
    /**
     * @Route("/favoris", name="favoris")
     * @IsGranted("ROLE_USER")
     */
    public function index(EntityManagerInterface $em): Response
    {
        //On récupère les cryptos fav d'un utilisateur
        //Les 1 doit être changer pour correspondre à l'id utilisateur
        //A voir comment on le récupère.
        $user = $this->getUser();
        $repositoryFav = $em->getRepository(Favori::class);
        $favoris = $repositoryFav->findAllByUtilisateur($user->getUsername());
        $Cryptos = array();
        foreach ($favoris as $fav){
            $id = $fav->getIdCrypto();
            $repositoryCrypto = $em->getRepository(Crypto::class);
            $crypto = $repositoryCrypto->findCryptoById($id);
            array_push($Cryptos,$crypto);
        }

        return $this->render('favoris/index.html.twig', [
            'cryptos' => $Cryptos,
        ]);
    }

    /**
     * @Route("/add/{name}/{idCrypto}", name="addFavori")
     */
    public function addFavori($name, $idCrypto): Response
    {
        $user = $this->getUser();
        $favori = new Favori();
        $favori->setUtilisateur($user = $user->getUsername());
        $favori->setIdCrypto($idCrypto);

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($favori);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();
        //On Appel la methode qui ajoute la crypte dans les favorie dans la crypto.
        //Récupérer plus d'infos avec une autre requete sur l'api via le nom en paramètre
        return $this->render('favoris/AjouteAuFavori.html.twig', ['NomCrypto' => $name]);
    }

    /**
     * @Route("/delete/{name}/{idCrypto}", name="deleteFavori")
     */
    public function deleteFavori($name, $idCrypto): Response
    {
        // get EntityManager
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $favCrypto = $em->getRepository(Favori::class)->findBy(['id_crypto' => $idCrypto, 'utilisateur_id' =>$user->getUsername()]);
        $em->remove($favCrypto[0]);
        $em->flush();
        return $this->render('favoris/removeFavori.html.twig', ['NomCrypto' => $name]);
    }



}
