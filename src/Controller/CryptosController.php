<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Crypto;
use App\Entity\Favori;
use App\Form\CommentaireType;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Array_;
use PHPUnit\Util\Json;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Tests\Compiler\J;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CryptosController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function accueil()
    {
        return $this->render('cryptos/accueil.html.twig');
    }

    /**
     * @Route("/{_locale}/marketCapRank", name="marketCapRank")
     */
    public function marketCapRank(EntityManagerInterface $em)
    {
        $repositoryCrypto = $em->getRepository(Crypto::class);

        $resultClass = $repositoryCrypto->findall();
        $Inf50 = array();
        $Sup50Inf500 = array();
        $Sup500Inf1M = array();
        $Sup1M = array();
        foreach ($resultClass as $crypto ){
            if ($crypto->getMarketCap() < 50000000){
                array_push($Inf50,$crypto);
            }
            if ($crypto->getMarketCap() >= 50000000 & $crypto->getMarketCap() < 500000000){
                array_push($Sup50Inf500,$crypto);
            }
            if ($crypto->getMarketCap() >= 500000000 & $crypto->getMarketCap() < 1000000000){
                array_push($Sup500Inf1M,$crypto);
            }
            if ($crypto->getMarketCap() >= 1000000000){
                array_push($Sup1M,$crypto);
            }
    }
        $Inf50Count = count($Inf50);
        $Sup50Inf500Count = count($Sup50Inf500);
        $Sup500Inf1MCount = count($Sup500Inf1M);
        $Sup1MCount = count($Sup1M);

        return $this->render('cryptos/marketCapRank.html.twig',['Inf50' => $Inf50,
            'Sup50Inf500' => $Sup50Inf500,
            'Sup500Inf1M' => $Sup500Inf1M,
            'Sup1M' => $Sup1M,
            'Sup1MCount' =>$Sup1MCount ,
            'Sup500Inf1MCount' =>$Sup500Inf1MCount ,
            'Sup50Inf500Count' =>$Sup50Inf500Count ,
            'Inf50Count' =>$Inf50Count ,
            ]);
    }


    /**
     * @Route("/{_locale}/", name="cryptos_list")
     * @return Response
     */
    public function list(EntityManagerInterface $em): Response
    {
        // Note à Mathieu avec coins/market on peut  triée par catégorie
        // faut juste trouver comment seulement refresh la table avec les infos.

        //On enregistre dans la Base de donnée les données des cryptos.
        $this->MajBdd();

        //On récupère tout dans la BDD.
        $cryptos = array();
        $repositoryCrypto = $em->getRepository(Crypto::class);
        if ($this->RechercheAvecFiltre()) {
            //Vérifier si les filtres ne sont pas à null.
            //Faut mettre les champ du where ici
            if ($_POST["inputMin"] != null) {
                $inputMin = $_POST["inputMin"];
            } else {
                $inputMin = 0;
            }
            if ($_POST["inputMax"] != null) {
                $inputMax = $_POST["inputMax"];
            } else {
                $inputMax = 100000;
            }
            if ($_POST["taux"] != null) {
                $taux = $_POST["taux"];
            } else {
                $taux = -200;
            }
            if ($_POST["circulation"] != null) {
                $circulation = $_POST["circulation"];
            } else {
                $circulation = -1;
            }
            $cryptos = $repositoryCrypto->filtreCrypto($inputMin, $inputMax, $taux, $circulation);
        } else {
            $cryptos = $repositoryCrypto->findAll();
        }
        $listCat = $this->getListCategorie();

        //On envoie la liste à la vue
        return $this->render('cryptos/index.html.twig', ['cryptos' => $cryptos, 'listcat' => $listCat, 'cat' => ""]);
    }

    public function RechercheAvecFiltre(): bool
    {
        if (isset($_POST["inputMin"]) or isset($_POST["inputMax"]) or isset($_POST["taux"]) or isset($_POST["circulation"])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @Route("/{_locale}/rechercheAvance", name="rechercheAvance")
     * @return Response
     */
    public function rechercheAvance(EntityManagerInterface $em): Response
    {
        //On récupère tout dans la BDD.
        $cryptosPrix = array();
        array_push($cryptosPrix,$this->pourcentageAnnee('bitcoin'));
        array_push($cryptosPrix,$this->pourcentageAnnee('ethereum'));
        //On envoie la liste à la vue
        return $this->render('cryptos/rechercheAvancee.html.twig',['cryptosPrix' => $cryptosPrix]);
    }

    public function pourcentageAnnee($nom):array {
        $url = 'https://api.coingecko.com/api/v3/coins/'.$nom.'/market_chart';
        $parameters = [
            'vs_currency' => 'usd',
            'days' => 365,
            'interval' => 'daily',
        ];

        $headers = [
            'Accepts: application/json'
        ];
        $qs = http_build_query($parameters); // query string encode the parameters
        $request = "{$url}?{$qs}"; // create the request URL


        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,            // set the request URL
            CURLOPT_HTTPHEADER => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response = curl_exec($curl); // Send the request, save the response
        if ($response) {

            $tabPrices = json_decode($response,true);
            $tabPrices = $tabPrices["prices"];

        }
        $max = $tabPrices[0][1];
        $min = $tabPrices[0][1];
        foreach ($tabPrices as $prix){
            if ($prix[1] > $max){
                $max = $prix[1];
            }else if($prix[1] < $min){
                $min = $prix[1];
            }
        }
        $pourcentage = $max/$min *100;
        curl_close($curl); // Close request

        $mesPrix = ['name'=> $nom, 'pourcentage' => $pourcentage,'min'=>$min,'max'=>$max];

        return $mesPrix;
    }
    /**
     * @Route("/{_locale}/cat/{cat}", name="cryptos_list_cat")
     * @return Response
     */
    public function cryptosCat($cat, EntityManagerInterface $em): Response
    {
        // Note à Mathieu avec coins/market on peut  triée par catégorie
        // faut juste trouver comment seulement refresh la table avec les infos.

        //On récupère tout dans la BDD.
        $cryptos = array();
        $cryptos = $this->findCryptosCat($cat);
        $listCat = $this->getListCategorie();

        //On envoie la liste à la vue
        return $this->render('cryptos/index.html.twig', ['cryptos' => $cryptos, 'listcat' => $listCat, 'cat' => $cat]);
    }


    public function getListCategorie(): array
    {
        $url = 'https://api.coingecko.com/api/v3/coins/categories/list';
        $parameters = [];

        $headers = [
            'Accepts: application/json'
        ];
        $qs = http_build_query($parameters); // query string encode the parameters
        $request = "{$url}?{$qs}"; // create the request URL


        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,            // set the request URL
            CURLOPT_HTTPHEADER => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response = curl_exec($curl); // Send the request, save the response
        $listCat = array();
        if ($response) {
            foreach (json_decode($response, true) as $r) {
                $cat = $r["category_id"];
                array_push($listCat, $cat);
            }
        }

        curl_close($curl); // Close request

        return $listCat;
    }

    public function findCryptosCat($cat): array
    {
        $url = 'https://api.coingecko.com/api/v3/coins/markets';
        $parameters = [
            'vs_currency' => 'usd',
            'order' => 'market_cap_desc',
            'per_page' => '250',
            'page' => 1,
            'category' => $cat,
            'price_change_percentage' => '1h,24h,7d,30d,1y'
        ];

        $headers = [
            'Accepts: application/json'
        ];
        $qs = http_build_query($parameters); // query string encode the parameters
        $request = "{$url}?{$qs}"; // create the request URL


        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,            // set the request URL
            CURLOPT_HTTPHEADER => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response = curl_exec($curl); // Send the request, save the response
        $Cryptos = array();
        if ($response) {
            foreach (json_decode($response, true) as $r) {
                $Crypto = new Crypto();
                $Crypto->setNom($r["id"]);
                $Crypto->setSymbole($r["symbol"]);
                $Crypto->setPrixCreation($r["atl"]);
                $Crypto->setPrix($r["current_price"]);
                $Crypto->setImage($r["image"]);
                $Crypto->setDateMaj($r["last_updated"]);
                $Crypto->setDateCreation($r["atl_date"]);
                $Crypto->setCirculatingSupply((int)$r["circulating_supply"]);
                $Crypto->setChangePercentageSinceCreation($r["atl_change_percentage"]);
                $Crypto->setMarketCap($r["market_cap"]);
                $Crypto->setTotalVolume($r["total_volume"]);

                if ($r["market_cap_rank"] != null) {
                    $Crypto->setMarketCapRank($r["market_cap_rank"]);
                } else {
                    $Crypto->setMarketCapRank(0);
                }
                if ($r["max_supply"] != null) {
                    $Crypto->setMaxSupply($r["max_supply"]);
                } else {
                    $Crypto->setMaxSupply(0);
                }
                if ($r["price_change_percentage_24h"] != null) {
                    $Crypto->setPriceChangePercentage24h($r["price_change_percentage_24h"]);
                } else {
                    $Crypto->setPriceChangePercentage24h(0);
                }
                if ($r["price_change_percentage_1y_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage1yInCurrency(number_format($r["price_change_percentage_1y_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage1yInCurrency(0);
                }
                if ($r["price_change_percentage_1h_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage1hInCurrency(number_format($r["price_change_percentage_1h_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage1hInCurrency(0);
                }
                if ($r["price_change_percentage_24h_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage24hInCurrency(number_format($r["price_change_percentage_24h_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage24hInCurrency(0);
                }
                if ($r["price_change_percentage_7d_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage7dInCurrency(number_format($r["price_change_percentage_7d_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage7dInCurrency(0);
                }
                if ($r["price_change_percentage_30d_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage30dInCurrency(number_format($r["price_change_percentage_30d_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage30dInCurrency(0);
                }
                array_push($Cryptos, $Crypto);
            }
        }
        curl_close($curl); // Close request

        return $Cryptos;
    }

    public function MajBdd()
    {

        //On vide la BDD
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeUpdate($platform->getTruncateTableSQL('crypto', true /* whether to cascade */));


        //On ajoute dedasn via l'API
        $cryptosAPI = array();
        $cryptosAPI = $this->getAllFromAPI(1, $cryptosAPI);
        $cryptosAPI = $this->getAllFromAPI(2, $cryptosAPI);
    }

    /**
     * @Route("/{_locale}/detail/{name}", name="detail")
     */
    public function detail($name, EntityManagerInterface $em, Request $request): Response
    {
        $repositoryComms = $em->getRepository(Commentaire::class);
        $repositoryCrypto = $em->getRepository(Crypto::class);
        $crypto = $repositoryCrypto->findIdCrypto($name);
        $commentaires = $repositoryComms->FindAllByIdCrypto($name);
        $user = $this->getUser();

        $commentaire = new Commentaire();
        $commentaire->setDate(new \DateTime('now'));
        if ($this->getUser() !== null) {
            $commentaire->setAuteur($this->getUser()->getUsername());
        }
        $commentaire->setNomCrypto($name);

        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $commentaire = $form->getData();


            $em = $this->getDoctrine()->getManager();

            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($commentaire);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();

            return $this->render(
                'cryptos/commentAdd.html.twig',
                [
                    'name' => $name,
                ]);
        }
        //Récupérer plus d'infos avec une autre requete sur l'api via le nom en paramètre
        //Le 1 correspond à l'id de l'utilisateur à voir comment on peut le récupérer.
        if (isset($crypto[0])) {
            if ($user == null){
                return $this->render(
                    'cryptos/detail.html.twig',
                    [
                        'cryptos' => $this->getDetailCryptoAPI($name),
                        'idBDD' => $crypto[0]->getId(),
                        'isFav' => false,
                        'form' => $form->createView(),
                        'commentaires' => $commentaires
                    ]);
            }else{
                return $this->render(
                    'cryptos/detail.html.twig',
                    [
                        'cryptos' => $this->getDetailCryptoAPI($name),
                        'idBDD' => $crypto[0]->getId(),
                        'isFav' => $this->isFav($crypto[0]->getId(), $user->getUsername(), $em),
                        'form' => $form->createView(),
                        'commentaires' => $commentaires
                    ]);
            }

        } else {
            return $this->render(
                'cryptos/detail.html.twig',
                [
                    'cryptos' => $this->getDetailCryptoAPI($name),
                    'idBDD' => 0,
                    'isFav' => false,
                    'form' => $form->createView(),
                    'commentaires' => $commentaires
                ]);
        }
    }

    public function isFav($idCrypto, $idUtilisateur, $em): bool
    {
        $repositoryFav = $em->getRepository(Favori::class);
        $favori = $repositoryFav->findIfIsFav($idCrypto, $idUtilisateur);
        if ($favori == null) {
            return false;
        } else {
            return true;
        }
    }

    //retourne une Array de Cryptos.
    public function getAllFromAPI($numPage, $Cryptos): array
    {

        $url = 'https://api.coingecko.com/api/v3/coins/markets';
        $parameters = [
            'vs_currency' => 'usd',
            'order' => 'market_cap_desc',
            'per_page' => '250',
            'page' => $numPage,
            'price_change_percentage' => '1h,24h,7d,30d,1y'
        ];

        $headers = [
            'Accepts: application/json'
        ];
        $qs = http_build_query($parameters); // query string encode the parameters
        $request = "{$url}?{$qs}"; // create the request URL


        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,            // set the request URL
            CURLOPT_HTTPHEADER => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response = curl_exec($curl); // Send the request, save the response

        if ($response) {
            foreach (json_decode($response, true) as $r) {
                $Crypto = new Crypto();
                $Crypto->setNom($r["id"]);
                $Crypto->setSymbole($r["symbol"]);
                $Crypto->setPrixCreation($r["atl"]);
                $Crypto->setPrix($r["current_price"]);
                $Crypto->setImage($r["image"]);
                $Crypto->setDateMaj($r["last_updated"]);
                $Crypto->setDateCreation($r["atl_date"]);
                $Crypto->setCirculatingSupply((int)$r["circulating_supply"]);
                $Crypto->setChangePercentageSinceCreation($r["atl_change_percentage"]);
                $Crypto->setMarketCap($r["market_cap"]);
                $Crypto->setTotalVolume($r["total_volume"]);

                if ($r["market_cap_rank"] != null) {
                    $Crypto->setMarketCapRank($r["market_cap_rank"]);
                } else {
                    $Crypto->setMarketCapRank(0);
                }
                if ($r["max_supply"] != null) {
                    $Crypto->setMaxSupply($r["max_supply"]);
                } else {
                    $Crypto->setMaxSupply(0);
                }
                if ($r["price_change_percentage_24h"] != null) {
                    $Crypto->setPriceChangePercentage24h($r["price_change_percentage_24h"]);
                } else {
                    $Crypto->setPriceChangePercentage24h(0);
                }
                if ($r["price_change_percentage_1y_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage1yInCurrency(number_format($r["price_change_percentage_1y_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage1yInCurrency(0);
                }
                if ($r["price_change_percentage_1h_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage1hInCurrency(number_format($r["price_change_percentage_1h_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage1hInCurrency(0);
                }
                if ($r["price_change_percentage_24h_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage24hInCurrency(number_format($r["price_change_percentage_24h_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage24hInCurrency(0);
                }
                if ($r["price_change_percentage_7d_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage7dInCurrency(number_format($r["price_change_percentage_7d_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage7dInCurrency(0);
                }
                if ($r["price_change_percentage_30d_in_currency"] != null) {
                    $Crypto->setPriceChangePercentage30dInCurrency(number_format($r["price_change_percentage_30d_in_currency"], 2, '.', ''));
                } else {
                    $Crypto->setPriceChangePercentage30dInCurrency(0);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($Crypto);
                $em->flush();
                array_push($Cryptos, $Crypto);
            }
        }
        curl_close($curl); // Close request

        return $Cryptos;
    }

    //retourne une Array avec les details Cryptos.
    public function getDetailCryptoAPI($nomCryptos): array
    {

        $url = 'https://api.coingecko.com/api/v3/coins/' . $nomCryptos;
        $parameters = [];
        $headers = [
            'Accepts: application/json'
        ];
        $qs = http_build_query($parameters); // query string encode the parameters
        $request = "{$url}?{$qs}"; // create the request URL

        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,            // set the request URL
            CURLOPT_HTTPHEADER => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response = curl_exec($curl); // Send the request, save the response
        $cryptoDetail = array();
        if ($response) {
            $r = json_decode($response, true);
            $cryptoDetail = $r;

        }
        return $cryptoDetail;
    }
}
