<?php

namespace App\Entity;

use App\Repository\FavoriRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FavoriRepository::class)
 */
class Favori
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_crypto;

    /**
     * @ORM\Column(type="string")
     */
    private $utilisateur_id;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdCrypto(): ?int
    {
        return $this->id_crypto;
    }

    public function setIdCrypto(int $id_crypto): self
    {
        $this->id_crypto = $id_crypto;

        return $this;
    }

    public function getUtilisateur(): ?string
    {
        return $this->utilisateur_id;
    }

    public function setUtilisateur(string $utilisateur_id): self
    {
        $this->utilisateur_id = $utilisateur_id;

        return $this;
    }
}
