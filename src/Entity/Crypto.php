<?php

namespace App\Entity;

use App\Repository\CryptoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="crypto")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=CryptoRepository::class)
 */
class Crypto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $symbole;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="integer")
     */
    private $market_cap_rank;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date_maj;

    /**
     * @ORM\Column(type="float")
     */
    private $price_change_percentage_24h;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date_creation;

    /**
     * @ORM\Column(type="float")
     */
    private $prix_creation;

    /**
     * @ORM\Column(type="float")
     */
    private $change_percentage_since_creation;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $circulating_supply;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $maxSupply;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_change_percentage_1h_in_currency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_change_percentage_1y_in_currency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_change_percentage_24h_in_currency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_change_percentage_30d_in_currency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_change_percentage_7d_in_currency;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $market_cap;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $total_volume;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSymbole(): ?string
    {
        return $this->symbole;
    }

    public function setSymbole(string $symbole): self
    {
        $this->symbole = $symbole;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getMarketCapRank(): ?int
    {
        return $this->market_cap_rank;
    }

    public function setMarketCapRank(int $market_cap_rank): self
    {
        $this->market_cap_rank = $market_cap_rank;

        return $this;
    }

    public function getDateMaj(): string
    {
        return $this->date_maj;
    }

    public function setDateMaj(string $date_maj): self
    {
        $this->date_maj = $date_maj;

        return $this;
    }

    public function getPriceChangePercentage24h(): ?float
    {
        return $this->price_change_percentage_24h;
    }

    public function setPriceChangePercentage24h(float $price_change_percentage_24h): self
    {
        $this->price_change_percentage_24h = $price_change_percentage_24h;

        return $this;
    }

    public function getDateCreation(): string
    {
        return $this->date_creation;
    }

    public function setDateCreation(string $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getPrixCreation(): ?float
    {
        return $this->prix_creation;
    }

    public function setPrixCreation(float $prix_creation): self
    {
        $this->prix_creation = $prix_creation;

        return $this;
    }

    public function getChangePercentageSinceCreation(): ?float
    {
        return $this->change_percentage_since_creation;
    }

    public function setChangePercentageSinceCreation(float $change_percentage_since_creation): self
    {
        $this->change_percentage_since_creation = $change_percentage_since_creation;

        return $this;
    }

    public function getCirculatingSupply(): ?int
    {
        return $this->circulating_supply;
    }

    public function setCirculatingSupply(int $circulating_supply): self
    {
        $this->circulating_supply = $circulating_supply;

        return $this;
    }

    public function getMaxSupply(): int
    {
        return $this->maxSupply;
    }

    public function setMaxSupply(int $max_supply): self
    {
        $this->maxSupply = $max_supply;

        return $this;
    }

    public function getPriceChangePercentage1hInCurrency(): ?float
    {
        return $this->price_change_percentage_1h_in_currency;
    }

    public function setPriceChangePercentage1hInCurrency(?float $price_change_percentage_1h_in_currency): self
    {
        $this->price_change_percentage_1h_in_currency = $price_change_percentage_1h_in_currency;

        return $this;
    }

    public function getPriceChangePercentage1yInCurrency(): ?float
    {
        return $this->price_change_percentage_1y_in_currency;
    }

    public function setPriceChangePercentage1yInCurrency(?float $price_change_percentage_1y_in_currency): self
    {
        $this->price_change_percentage_1y_in_currency = $price_change_percentage_1y_in_currency;

        return $this;
    }

    public function getPriceChangePercentage24hInCurrency(): ?float
    {
        return $this->price_change_percentage_24h_in_currency;
    }

    public function setPriceChangePercentage24hInCurrency(?float $price_change_percentage_24h_in_currency): self
    {
        $this->price_change_percentage_24h_in_currency = $price_change_percentage_24h_in_currency;

        return $this;
    }

    public function getPriceChangePercentage30dInCurrency(): ?float
    {
        return $this->price_change_percentage_30d_in_currency;
    }

    public function setPriceChangePercentage30dInCurrency(?float $price_change_percentage_30d_in_currency): self
    {
        $this->price_change_percentage_30d_in_currency = $price_change_percentage_30d_in_currency;

        return $this;
    }

    public function getPriceChangePercentage7dInCurrency(): ?float
    {
        return $this->price_change_percentage_7d_in_currency;
    }

    public function setPriceChangePercentage7dInCurrency(?float $price_change_percentage_7d_in_currency): self
    {
        $this->price_change_percentage_7d_in_currency = $price_change_percentage_7d_in_currency;

        return $this;
    }

    public function getMarketCap(): ?int
    {
        return $this->market_cap;
    }

    public function setMarketCap(int $market_cap): self
    {
        $this->market_cap = $market_cap;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getTotalVolume(): ?int
    {
        return $this->total_volume;
    }

    public function setTotalVolume(int $total_volume): self
    {
        $this->total_volume = $total_volume;

        return $this;
    }
}
