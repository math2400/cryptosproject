<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220506095427 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE commentaire (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, date DATE NOT NULL, titre VARCHAR(255) NOT NULL, auteur LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE crypto (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, symbole VARCHAR(5) NOT NULL, prix DOUBLE PRECISION NOT NULL, image VARCHAR(255) DEFAULT NULL, market_cap_rank INT NOT NULL, date_maj VARCHAR(255) NOT NULL, price_change_percentage_24h DOUBLE PRECISION NOT NULL, date_creation VARCHAR(255) NOT NULL, prix_creation DOUBLE PRECISION NOT NULL, change_percentage_since_creation DOUBLE PRECISION NOT NULL, circulating_supply INT NOT NULL, max_supply INT DEFAULT NULL, price_change_percentage_1h_in_currency DOUBLE PRECISION DEFAULT NULL, price_change_percentage_1y_in_currency DOUBLE PRECISION DEFAULT NULL, price_change_percentage_24h_in_currency DOUBLE PRECISION DEFAULT NULL, price_change_percentage_30d_in_currency DOUBLE PRECISION DEFAULT NULL, price_change_percentage_7d_in_currency DOUBLE PRECISION DEFAULT NULL, market_cap BIGINT NOT NULL, categorie VARCHAR(255) NOT NULL, total_volume INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE favori (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT DEFAULT NULL, id_crypto INT NOT NULL, INDEX IDX_EF85A2CCFB88E14F (utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, role VARCHAR(50) NOT NULL, mdp VARCHAR(50) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE favori ADD CONSTRAINT FK_EF85A2CCFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE favori DROP FOREIGN KEY FK_EF85A2CCFB88E14F');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE crypto');
        $this->addSql('DROP TABLE favori');
        $this->addSql('DROP TABLE utilisateur');
    }
}
